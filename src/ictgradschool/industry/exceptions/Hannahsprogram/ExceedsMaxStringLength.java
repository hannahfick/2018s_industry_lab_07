package ictgradschool.industry.exceptions.Hannahsprogram;

public class ExceedsMaxStringLength extends Exception {

    public ExceedsMaxStringLength(String s) {
        super(s);
    }
}