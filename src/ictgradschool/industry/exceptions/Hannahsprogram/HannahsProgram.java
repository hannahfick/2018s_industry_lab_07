package ictgradschool.industry.exceptions.Hannahsprogram;


import ictgradschool.Keyboard;

import java.util.Scanner;

public class HannahsProgram {

    public static void main(String[] args) throws ExceedsMaxStringLength, InvalidWordException {

        System.out.println("Enter a string of no more than 100 characters: ");
        String character;
        character = Keyboard.readInput();

        String[] array = character.split("\\s+");

        String output = "";
        for (String s : array) {

            char first = s.charAt(0);
            int x = Character.getNumericValue(first);

            if (first >= '0' && first <= '9') {
                throw new InvalidWordException("String contains invalid words");
            }

            if (s.length() > 100) {
                throw new ExceedsMaxStringLength("String is more than 100 characters");
            }

            output += first + " ";
        }

        System.out.println("You have entered: " + output.trim());


    }
}



