package ictgradschool.industry.exceptions.simpleexceptions;

import ictgradschool.Keyboard;

import java.util.Scanner;


public class SimpleExceptions {
    public static void main(String[] args) {
        SimpleExceptions exceptions = new SimpleExceptions();
        exceptions.handlingException();

        try {
            exceptions.Question3();
        } catch (StringIndexOutOfBoundsException c) {
            System.out.println("Caught exception: " + c);
        }

        try {
            exceptions.Question4();
        } catch (ArrayIndexOutOfBoundsException c) {
            System.out.println("Caught exception: " + c);
        }

    }

    /**
     * The following tries to divide using two user input numbers, but is
     * prone to error.
     */
    public void handlingException() {
        Scanner sc = new Scanner(System.in);


        System.out.print("Enter the first number: ");
        String str1 = sc.next();
        int num1 = Integer.parseInt(str1);

        System.out.print("Enter the second number: ");
        String str2 = sc.next();
        int num2 = Integer.parseInt(str2);

        // Output the result
        try {
            System.out.println("The division of " + num1 + " over " + num2 + " is " + (num1 / num2) + "\n");
        } catch (ArithmeticException nfe) {
            System.out.println("Caught exception: " + nfe);

        }
    }

    public void Question3() throws StringIndexOutOfBoundsException {
        String s = "another day of java- aka Devil Code";
        System.out.println(s.charAt(100));
        //Write some Java code which throws a StringIndexOutOfBoundsException
    }

    public void Question4() {
        int[] anArray;
        anArray = new int [7];
        int array = anArray[77];
        //Write some Java code which throws a ArrayIndexOutOfBoundsException
    }
}