package ictgradschool.industry.exceptions.arraysandexceptions;

public class IndexToolLowException extends Exception {
    public IndexToolLowException() {
    }

    public IndexToolLowException(String message) {
        super(message);
    }

    public IndexToolLowException(String message, Throwable cause) {
        super(message, cause);
    }

    public IndexToolLowException(Throwable cause) {
        super(cause);
    }
}
