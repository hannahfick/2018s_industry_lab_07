package ictgradschool.industry.exceptions.arraysandexceptions;

public class IndexToolHighException extends Exception {
    public IndexToolHighException() {
    }

    public IndexToolHighException(String message) {
        super(message);
    }

    public IndexToolHighException(String message, Throwable cause) {
        super(message, cause);
    }

    public IndexToolHighException(Throwable cause) {
        super(cause);
    }
}
